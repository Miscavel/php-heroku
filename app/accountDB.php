<?php
 
namespace PostgreSQL;
 
class AccountDB
{
    private $pdo;

    public function __construct($pdo) 
    {
        $this->pdo = $pdo;
    }

    public function all() 
    {
        $stmt = $this->pdo->query('SELECT user_id, username, password, email, created_on  '
                                    . 'FROM account '
                                    . 'ORDER BY user_id');
        $accounts = [];
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) 
        {
            $accounts[] = 
            [
                'user_id' => $row['user_id'],
                'username' => $row['username'],
                'password' => $row['password'],
                'email' => $row['email'],
                'created_on' => $row['created_on']
            ];
        }
        return $accounts;
    }
}
?>